FROM alpine:3.9

RUN apk update && apk add bash curl jq

COPY pipe /usr/bin/
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/usr/bin/pipe.sh"]
