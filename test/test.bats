#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/slack-notify"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE} .
}

teardown() {
  rm -rf $(pwd)/tmp/pipe-*.txt
}

@test "Test message is sent successfully" {
    run docker run \
        -e MESSAGE="Pipelines is awesome!" \
        -e WEBHOOK_URL=$WEBHOOK_URL \
        -e BITBUCKET_REPO_OWNER="$BITBUCKET_REPO_OWNER" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]

    request=$(cat $(pwd)/tmp/pipe-*.txt)

    [ "$request" = "ok" ]

}

@test "Test message containing double quotes is sent successfully" {
    run docker run \
        -e MESSAGE='"Pipelines is awesome!"' \
        -e WEBHOOK_URL=$WEBHOOK_URL \
        -e BITBUCKET_REPO_OWNER="$BITBUCKET_REPO_OWNER" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]

    request=$(cat $(pwd)/tmp/pipe-*.txt)

    [ "$request" = "ok" ]
}

@test "Test debug flag" {
    run docker run \
        -e MESSAGE="Pipelines is awesome!" \
        -e WEBHOOK_URL=$WEBHOOK_URL \
        -e DEBUG="true" \
        -e BITBUCKET_REPO_OWNER="$BITBUCKET_REPO_OWNER" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 ]]
    [[ "$output" =~ "Enabling debug mode." ]]
    [[ "$output" =~ "< HTTP/2 200" ]]

    request=$(cat $(pwd)/tmp/pipe-*.txt)

    [[ "$request" = "ok" ]]

}


